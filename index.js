const express = require('express')
var cors = require('cors');
const app = express()
app.use(cors());
app.get('/', (req, res) => res.send('Hello World!'))
app.use(express.static(__dirname + '/public'));
app.listen(3300, () => console.log('Example app listening on port 3300!'))
